#include <stdio.h>
#include <math.h>
void output(float d)
{
    printf("the distance is:%f\n",d);
}
void dist(float x1,float y1,float x2,float y2,float *d)
{
    *d=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
}
void input(float *x1,float *y1,float *x2,float *y2)
{
    printf("enter first point:\n");
    scanf("%f%f",x1,y1);
    printf("enter second point:\n");
    scanf("%f%f",x2,y2);
}
int main()
{
    float x1,x2,y1,y2;
    float distance;
    input(&x1,&y1,&x2,&y2);
    dist(x1,y1,x2,y2,&distance);
    output(distance);
    return 0;
}
