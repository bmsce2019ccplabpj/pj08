#include<stdio.h>
void output(int s)
{
    printf("The sum of digits is %d\n",s);
}
void sum_num(int num,int *r)
{
    int a;*r=0;
    while(num!=0)
    {
        a=num%10;
        *r=*r+a;
        num=num/10;
    }
}
void input(int *n)
{
    printf("enter a number whose sum of digits should be found:\n");
    scanf("%d",n);
}
int main()
{
    int num,sum;
    input(&num);
    sum_num(num,&sum);
    output(sum);
    return 0;
}