#include<stdio.h>
void output(int a,int b)
{

    printf("After swapping\n");
    printf("The numbers are:\n");
    printf("a=%d and b=%d\n",a,b);

}
void swap(int *a,int *b)
{
    *a=*a+*b;
    *b=*a-*b;
    *a=*a-*b;
}
void input(int *a,int *b)
{
    printf("enter first number a:\n");
    scanf("%d",a);
    printf("enter second number b:\n");
    scanf("%d",b);
}
int main()
{
    int a,b;
    input(&a,&b);
    printf("The numbers are:\n");
    printf("a=%d and b=%d\n",a,b);
    swap(&a,&b);
    output(a,b);
    return 0;
}