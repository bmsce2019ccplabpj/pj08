#include<stdio.h>
void output(int e,int pos)
{
    if(pos==-1)
        printf("Element not found\n");
    else 
        printf("The position of %d is %d\n",e,pos);
}

void search(int n,int x[n],int e,int *pos)
{
    *pos=-1;
    for(int i=0;i<n;i++)
    {
        if(x[i]==e)
            *pos=i;
    }
}
void input(int n,int x[n],int *e)
{
    for(int i=0;i<n;i++)
    {
        printf("Enter element\n");
        scanf("%d",&x[i]);
    }
    printf("Enter element to be searched:\n");
    scanf("%d",e);
}
int main()
{
    int n,e,pos;
    printf("Enter the number of elements in the array\n");
    scanf("%d",&n);
    int a[n];
    input(n,a,&e);
    search(n,a,e,&pos);
    output(e,pos);
    return 0;
}


