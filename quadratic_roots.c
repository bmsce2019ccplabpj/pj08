#include<stdio.h>
#include<math.h>
void output(float a,float b,float c,float d,int r)
{
    float r1,r2;
    switch(r)
    {
        case 1:r1=(-b)/(2*a);
            r2=r1;
            printf("the roots are real and equal\n the roots are %f and %f\n",r1,r2);
            break;
        case 2:r1=((-b)+sqrt(d))/(2*a);
            r2=((-b)-sqrt(d))/(2*a);
            printf("the roots are real and distinct\n the roots are %f and %f\n",r1,r2);
            break;
        default:printf("the roots are imaginary\n");
    }
} 



void solve(float a,float b,float c,int *r,float *d)
{
    *d=pow(b,2)-(4*a*c);
    if(*d==0)
    { 
        *r=1;
    }
    else if(*d>0)
    {
        *r=2;
    }
    else
    { 
        *r=0;
    }
}


void input(float *a,float *b,float *c)
{
    printf("Enter coefficients for quadratic equation\n");
    scanf("%f%f%f",a,b,c);
}




int main()
{
    float a,b,c,d;int r;
    input(&a,&b,&c);
    solve(a,b,c,&r,&d);
    output(a,b,c,d,r);
    return 0;
}