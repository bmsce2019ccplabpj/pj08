#include<stdio.h>


void output(int e,int key)
{
    if (key==0)
    {
        printf("the element is not found\n");
    }
    else
        printf("the position of element %d is %d\n",e,(key+1));  
}



void compute(int n,int x[n],int e,int *k)
{
    int beg=0;
    int end=n-1;
    while(beg<=end)
    {
        int mid=(beg+end)/2;
        if(x[mid]==e)
        {
            *k=mid;
            break;
        }
        else if(x[mid]>e)
        {
            beg=mid+1;
        }
        else if(x[mid]<e)
        {
            end=mid-1;
        }
        else
            *k=0;
    }
}   



void input(int n,int x[n],int *e)
{
    for(int i=0;i<n;i++)
    { 
        printf("enter the element\n");
        scanf("%d",&x[i]);
    }
    printf("enter the element to be searched\n");
    scanf("%d",e);   
}



int main()
{
    int n,e,key;
    printf("enter the number of elements in the array\n");
    scanf("%d",&n);
    int a[n];
    input(n,a,&e);
    compute(n,a,e,&key);
    output(e,key);
    return 0;
}
