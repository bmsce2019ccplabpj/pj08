#include<stdio.h>
void output(int t)
{
    printf("the time in minutes is %d\n",t);
}
void convert(int h,int m,int *t)
{
    *t=(h*60)+m;
}
void input(int *h,int *m)
{
    printf("enter time in the format (hours) (minutes)\n");
    scanf("%d%d",h,m);
}
int main()
{
    int hours,minutes,time;
    input(&hours,&minutes);
    convert(hours,minutes,&time);
    output(time);
    return 0;
}