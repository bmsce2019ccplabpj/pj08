#include <stdio.h>
int main()
{
    char c;
    FILE *fp;
    fp=fopen("INPUT.txt","w");
    printf("Enter the data to be written in the file\n");
    while((c=getchar())!=EOF)
    {
        fputc(c,fp);
    }
    fclose(fp);
    fp=fopen("INPUT.txt","r");
    printf("The file contents are:\n");
    while((c=fgetc(fp))!=EOF)
    {
        printf("%c",c);
    }
    fclose(fp);
    return 0;
}