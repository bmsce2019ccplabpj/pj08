#include<stdio.h>
void output(int r,int n)
{
  if(r==n)
     { 
         printf("The reverse number is %d\n",r);
         printf("The number is palindrome\n");
     }
  else
     {
         printf("The reverse number is %d\n",r);
         printf("The number is not palindrome\n");
     }
}




void rev_num(int num,int *r)
{
    int a;*r=0;
    
      for(;num!=0;num=num/10)
       {
           a=num%10;
           *r=(*r*10)+a;
       }
}




void input(int *n)
{
  printf("enter a number to be reversed:\n");
  scanf("%d",n);
}




int main()
{
    int num,rev;
    input(&num);
    rev_num(num,&rev);
    output(rev,num);
    return 0;
}