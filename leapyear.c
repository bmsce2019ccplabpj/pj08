#include<stdio.h>
  void leapyear(int yr,int *f)
  {
    if(yr%4==0 && yr%100!=0)
    {
     *f=1;
    }
    else if(yr%400==0)
    {
     *f=2;
    }
    else
    {
     *f=0;
    }
 }
 
 
 
 
 void input(int *yr)
 {
 
 printf("Enter the year\n");
 scanf("%d",yr);
 
 }
 
 
 
 
int main()
{
   int year,flag;
   input(&year);
   leapyear(year,&flag);
   
   if(flag==1)
  {
   printf("The year %d is a leap year\n",year);
  }
  else if(flag==2)
  {
   printf("The year %d is a century leap year\n",year);
  }
  else
  {
   printf("The year %d is not a leap year\n",year);
  }
  return 0;
}